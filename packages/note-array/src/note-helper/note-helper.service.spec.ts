import { Test, TestingModule } from '@nestjs/testing';
import { NoteHelperService } from './note-helper.service';

describe('NoteHelperService', () => {
  let service: NoteHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NoteHelperService],
    }).compile();

    service = module.get<NoteHelperService>(NoteHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
