import { Injectable } from '@nestjs/common';
import { NoteDTO } from '../interfaces/note.dto';

@Injectable()
export class NoteHelperService {
  private noteList: NoteDTO[] = [];

  create(note: NoteDTO) {
    this.noteList.push(note);
  }

  read() {
    return this.noteList;
  }

  delete(uuid: string) {
    this.noteList = this.noteList.filter(note => note.uuid !== uuid);
  }

  update(note: NoteDTO) {
    this.noteList = this.noteList.filter(n => {
      if (n.uuid === note.uuid) {
        n.title = note.title;
        n.message = note.message;
      }
      return n;
    });
  }
}
