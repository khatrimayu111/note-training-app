import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NoteHelperService } from './note-helper/note-helper.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, NoteHelperService],
})
export class AppModule {}
