export class NoteDTO {
  uuid: string;
  title: string;
  message: string;
}
