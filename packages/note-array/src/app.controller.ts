import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { NoteDTO } from './interfaces/note.dto';
import * as uuidv4 from 'uuid/v4';
import { NoteHelperService } from './note-helper/note-helper.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly noteHelperService: NoteHelperService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('create')
  createNote(@Body() note: NoteDTO) {
    note.uuid = uuidv4();
    this.noteHelperService.create(note);
  }

  @Get('read')
  getNote() {
    return this.noteHelperService.read();
  }

  @Post('update')
  updateNote(@Body() note: NoteDTO) {
    this.noteHelperService.update(note);
  }

  @Post('delete')
  deleteNote(@Body('uuid') uuid: string) {
    this.noteHelperService.delete(uuid);
  }
}
