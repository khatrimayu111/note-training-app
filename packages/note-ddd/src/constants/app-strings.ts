export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const AUTHORIZATION = 'authorization';
export const SERVICE = 'note-ddd';
export const PUBLIC = 'public';
export const APP_NAME = 'note-ddd';
export const SWAGGER_ROUTE = 'api-docs';
