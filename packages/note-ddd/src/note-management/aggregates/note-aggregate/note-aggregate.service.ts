import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { NoteDto } from '../../entities/note/note.dto';
import * as uuidv4 from 'uuid/v4';
import { NoteCreatedEvent } from '../../events/note-created/note-created.event';
import { NoteService } from '../../entities/note/note.service';
import { EditNoteDto } from '../../entities/propose-edit/edit-note.dto';
import { NoteEditProposedEvent } from '../../events/note-edit-proposed/note-edit-proposed.event';
import { ValidateAlreadyProposedEditService } from '../../policies/validate-already-proposed-edit/validate-already-proposed-edit.service';
import { ValidateNoteService } from '../../policies/validate-note/validate-note.service';

@Injectable()
export class NoteAggregateService extends AggregateRoot {
  constructor(
    private readonly noteService: NoteService,
    private readonly validateAlreadyProposedEditService: ValidateAlreadyProposedEditService,
    private readonly validateNoteService: ValidateNoteService,
  ) {
    super();
  }
  createNote(note: NoteDto, req) {
    note.uuid = uuidv4();

    this.apply(new NoteCreatedEvent(note));
  }

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(limit);
    return this.noteService.list(offset, limit);
  }

  async proposeEditNote(note: EditNoteDto) {
    note.uuid = uuidv4();
    await this.validateNoteService.validateNote(note.noteUuid);
    await this.validateAlreadyProposedEditService.validateAlreadyProposedEdit(
      note.noteUuid,
    );

    this.apply(new NoteEditProposedEvent(note));
  }
}
