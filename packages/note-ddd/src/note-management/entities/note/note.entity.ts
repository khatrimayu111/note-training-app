import { BaseEntity, ObjectID, ObjectIdColumn, Column, Entity } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class Note extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  title: string;

  @Column()
  message: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
