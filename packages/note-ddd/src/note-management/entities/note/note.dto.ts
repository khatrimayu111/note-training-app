import { IsNotEmpty, IsOptional } from 'class-validator';

export class NoteDto {
  @IsOptional()
  uuid: string;

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  message: string;
}
