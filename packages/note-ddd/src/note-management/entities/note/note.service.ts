import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { Repository } from 'typeorm';
import { NoteDto } from './note.dto';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private readonly noteRepository: Repository<Note>,
  ) {}

  async create(note: NoteDto) {
    const noteObject = new Note();
    Object.assign(noteObject, note);
    return await noteObject.save();
  }

  async findNote(uuid: string) {
    const foundNote = await this.noteRepository.findOne({ uuid });
    return foundNote;
    // if( !foundNote ){
    //   throw new NotFoundException('Note not found');
    // }
  }

  // async update(uuid: string, note: NoteDto) {
  //   const foundNote = await this.noteRepository.findOne({ uuid });
  //   foundNote.title = note.title;
  //   foundNote.message = note.message;
  //   return await this.noteRepository.save(foundNote);
  // }

  // async delete(uuid) {
  //   const foundNote = await this.noteRepository.findOne({ uuid });
  //   await foundNote.remove();
  // }

  async list(skip: number, take: number) {
    const providers = await this.noteRepository.find({ skip, take });

    return {
      docs: providers,
      length: await this.noteRepository.count(),
      offset: skip,
    };
  }
}
