import { CreateNoteHandler } from './create-note/create-note.handler';
import { ProposeNoteEditHandler } from './propose-note-edit/propose-note-edit.handler';

export const CommandManager = [CreateNoteHandler, ProposeNoteEditHandler];
