import { Module } from '@nestjs/common';
import { NoteController } from './controllers/note/note.controller';
import { AggregateManager } from './aggregates';
import { CommandManager } from './commands';
import { EventManager } from './events';
import { NoteEntitiesModule } from './entities/entities.module';
import { QueryManager } from './queries';
import { ValidateAlreadyProposedEditService } from './policies/validate-already-proposed-edit/validate-already-proposed-edit.service';
import { ValidateNoteService } from './policies/validate-note/validate-note.service';

@Module({
  controllers: [NoteController],
  imports: [NoteEntitiesModule],
  providers: [
    ValidateAlreadyProposedEditService,
    ValidateNoteService,
    ...AggregateManager,
    ...CommandManager,
    ...EventManager,
    ...QueryManager,
  ],
  exports: [
    NoteEntitiesModule,
    ValidateAlreadyProposedEditService,
    ValidateNoteService,
  ],
})
export class NoteManagementModule {}
