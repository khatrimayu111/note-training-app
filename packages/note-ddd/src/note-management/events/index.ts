import { NoteCreatedHandler } from './note-created/note-created.handler';
import { NoteEditProposedHandler } from './note-edit-proposed/note-edit-proposed.handler';

export const EventManager = [NoteCreatedHandler, NoteEditProposedHandler];
