import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { NoteEditProposedEvent } from './note-edit-proposed.event';
import { ProposeEditService } from '../../entities/propose-edit/propose-edit.service';

@EventsHandler(NoteEditProposedEvent)
export class NoteEditProposedHandler
  implements IEventHandler<NoteEditProposedEvent> {
  constructor(private readonly proposedEditService: ProposeEditService) {}

  async handle(event: NoteEditProposedEvent) {
    const { note } = event;
    await this.proposedEditService.createProposalForNoteEdit(note);
  }
}
